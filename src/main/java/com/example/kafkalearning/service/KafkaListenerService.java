package com.example.kafkalearning.service;

import com.example.kafkalearning.event.OrderRegistrationEvent;
import com.example.kafkalearning.model.OrderEntity;
import com.example.kafkalearning.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.retry.annotation.Backoff;
import org.springframework.stereotype.Service;

import java.net.SocketTimeoutException;

import static com.example.kafkalearning.config.KafkaTopicConfig.TOPIC_ERROR_MESSAGES;
import static com.example.kafkalearning.config.KafkaTopicConfig.TOPIC_ORDER_EVENTS;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaListenerService {
    private final KafkaTemplate<String, Object> kafkaTemplate;

    @KafkaListener(topics = TOPIC_ORDER_EVENTS)
    @RetryableTopic(
            backoff = @Backoff(value = 3000L),
            attempts = "5",
            include = NullPointerException.class)
    public void listen(ConsumerRecord<String, OrderRegistrationEvent> record) {
        log.info("Message received: key - {}, value - {}", record.key(), record.value());
        final OrderRegistrationEvent value = record.value();
        saveOrder(value);
    }

    //For understanding dlq working principle
//    @KafkaListener(topics = TOPIC_ORDER_EVENTS)
//    public void listen1(ConsumerRecord<String, OrderRegistrationEvent> record) {
//        final OrderRegistrationEvent value = record.value();
//        try {
//            if ( value.getCustomerName().isEmpty()) {
//                throw new IllegalArgumentException("Customer name is missing");
//            }
//            log.info("Ordered by {}", value.getCustomerName());
//        } catch (Exception e) {
//            kafkaTemplate.send(TOPIC_ERROR_MESSAGES,record.value());
//            log.error("Failed to process message, sent to DLQ: key - {}, value - {}", record.key(), record.value(), e);
//        }
//        log.info("Message received: key - {}, value - {}", record.key(), record.value());
//    }


    public void saveOrder(OrderRegistrationEvent orderRegistrationEvent) {
        log.info("Order regitered from {}", orderRegistrationEvent.getCustomerName());
        throw new NullPointerException("exception");
    }
}
