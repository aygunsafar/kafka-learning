package com.example.kafkalearning.service;

import com.example.kafkalearning.dto.OrderRequestDto;
import com.example.kafkalearning.dto.OrderResponseDto;
import com.example.kafkalearning.model.OrderEntity;
import com.example.kafkalearning.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ModelMapper modelMapper;
    @Override
    public OrderResponseDto createOrder(OrderRequestDto requestDto) {
        OrderEntity order= modelMapper.map(requestDto, OrderEntity.class);
        return modelMapper.map(orderRepository.save(order),OrderResponseDto.class);
    }

    @Override
    public OrderResponseDto getOrder(Long id) {
        OrderEntity order = orderRepository.findById(id).orElseThrow(()->new RuntimeException("Order is not exixt"));
        return modelMapper.map(order,OrderResponseDto.class);
    }
}
