package com.example.kafkalearning.service;

import com.example.kafkalearning.dto.OrderRequestDto;
import com.example.kafkalearning.event.OrderRegistrationEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static com.example.kafkalearning.config.KafkaTopicConfig.TOPIC_ORDER_EVENTS;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProduceService {
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final ModelMapper modelMapper;

    public void produce(OrderRequestDto requestDto) {
        log.info("Order info " + requestDto);
        final OrderRegistrationEvent orderRegistrationEvent = modelMapper.map(requestDto, OrderRegistrationEvent.class);
        kafkaTemplate.send(TOPIC_ORDER_EVENTS,"5",orderRegistrationEvent);
    }

}
