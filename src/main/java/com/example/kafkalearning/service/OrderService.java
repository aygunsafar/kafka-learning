package com.example.kafkalearning.service;

import com.example.kafkalearning.dto.OrderRequestDto;
import com.example.kafkalearning.dto.OrderResponseDto;

public interface OrderService {
    OrderResponseDto createOrder(OrderRequestDto requestDto);
    OrderResponseDto getOrder(Long id);

}
