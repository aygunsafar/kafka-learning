package com.example.kafkalearning.config;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;


@Configuration
public class KafkaTopicConfig {
    @Value("${kafka.bootstrap:localhost:9092}")
    private String bootstrapServers;

    public static final String TOPIC_ORDER_EVENTS = "order-events";
    public static final String TOPIC_ERROR_MESSAGES = "error-occured-messages";

    @Bean
    public NewTopic orderTopic() {
        return TopicBuilder.name(TOPIC_ORDER_EVENTS)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic errorMessageTopic(){
        return TopicBuilder.name(TOPIC_ERROR_MESSAGES)
                .partitions(3)
                .replicas(1)
                .build();
    }


    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        return new KafkaAdmin(configs);
    }

}
