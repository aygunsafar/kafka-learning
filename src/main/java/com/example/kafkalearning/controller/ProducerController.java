package com.example.kafkalearning.controller;

import com.example.kafkalearning.dto.OrderRequestDto;
import com.example.kafkalearning.event.OrderRegistrationEvent;
import com.example.kafkalearning.service.ProduceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

import static com.example.kafkalearning.config.KafkaTopicConfig.TOPIC_ORDER_EVENTS;

@RestController()
@RequestMapping("/producer")
@RequiredArgsConstructor
@Slf4j
public class ProducerController {

    private final ProduceService produceService;
    @PostMapping
    public void produce(@RequestBody OrderRequestDto requestDto){
        produceService.produce(requestDto);
    }
}
