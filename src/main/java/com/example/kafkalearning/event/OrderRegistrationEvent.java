package com.example.kafkalearning.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderRegistrationEvent {
    private String customerName;
    private String product;
    private Integer quantity;
}
