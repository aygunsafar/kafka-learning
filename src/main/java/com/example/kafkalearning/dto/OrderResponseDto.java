package com.example.kafkalearning.dto;

import lombok.Data;

import java.time.LocalDateTime;
@Data
public class OrderResponseDto {

    private Long id;
    private String customerName;
    private String product;
    private Integer quantity;
    private LocalDateTime orderDate;
}


